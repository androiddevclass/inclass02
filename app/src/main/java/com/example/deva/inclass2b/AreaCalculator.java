/*
Assignment : InClass2b
File Name: AreaCalculator.java
Names: Advait Athavale, Devdatta Kulkarni
 */
package com.example.deva.inclass2b;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class AreaCalculator extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_calculator);
        findViewById(R.id.calculateButton).setOnClickListener(this);
//        ( (RadioButton) findViewById(R.id.areaCircle)).setOnCheckedChangeListener(this);
//        ( (RadioButton) findViewById(R.id.areaRectangle)).setOnCheckedChangeListener(this);
//        ( (RadioButton) findViewById(R.id.areaTriangle)).setOnCheckedChangeListener(this);
//        ( (RadioButton) findViewById(R.id.areaSquare)).setOnCheckedChangeListener(this);

       ( (RadioButton) findViewById(R.id.clearAll)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
           @Override
           public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            ((TextView) findViewById(R.id.resultDisplayLabel)).setText("");
            ((EditText) findViewById(R.id.length1value)).setText("");
            ((EditText) findViewById(R.id.length2value)).setText("");
           }
       });

    }

    @Override
    public void onClick(View v) {

        String length2 = null;
        String length1 = ((EditText) findViewById(R.id.length1value)).getText().toString();
        if(((EditText) findViewById(R.id.length2value)).isEnabled()) {
            length2 = ((EditText) findViewById(R.id.length2value)).getText().toString();
        }


        if(!checkValidNumber(length1)){
            //((TextView) findViewById(R.id.resultDisplayLabel)).setText(R.string.invalidInput1);
            return;
        }

        switch (((RadioGroup) findViewById(R.id.shapeRadioGrp)).getCheckedRadioButtonId()){
            case R.id.areaCircle:
                ((EditText) findViewById(R.id.length2value)).setText("");
                ((TextView) findViewById(R.id.resultDisplayLabel)).setText(""+3.14* Double.parseDouble(length1)*Double.parseDouble(length1));
                break;
            case  R.id.areaRectangle:
                if(!checkValidNumber(length2)){
                    //((TextView) findViewById(R.id.resultDisplayLabel)).setText(R.string.invalidInput2);
                    return;
                }
                ((TextView) findViewById(R.id.resultDisplayLabel)).setText(""+Double.parseDouble(length1)*Double.parseDouble(length2));
                break;
            case  R.id.areaSquare:
                ((EditText) findViewById(R.id.length2value)).setText("");
                ((TextView) findViewById(R.id.resultDisplayLabel)).setText(""+Double.parseDouble(length1)*Double.parseDouble(length1));
                break;
            case  R.id.areaTriangle:
                if(!checkValidNumber(length2)){
                    //((TextView) findViewById(R.id.resultDisplayLabel)).setText(R.string.invalidInput2);
                    return;
                }
                ((TextView) findViewById(R.id.resultDisplayLabel)).setText(""+0.5* Double.parseDouble(length1)*Double.parseDouble(length2));
                break;

        }

    }

    private boolean checkValidNumber(String value){
        try {
            Double.parseDouble(value);
        }catch (Exception e){
            Toast.makeText(getApplicationContext(), R.string.invalidInput1, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
